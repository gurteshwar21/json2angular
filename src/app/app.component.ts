import { Component,OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { PaginationControls } from 'src/app/pagination-model';
import { UiService } from './ui.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public searchString: string;
  title = 'Angular Example';
  products: any = [];
  pageControl = new PaginationControls();
    //sorting
    key: string = 'name'; //set default
    reverse: boolean = false;
    
  constructor(private httpClient: HttpClient,
    public uiService: UiService
    
    ){}
  ngOnInit(){
    this.httpClient.get<any[]>("assets/data.json").subscribe(data =>{
      console.log(data);
      this.products = data;
      this.pageControl.count = this.products.length;
    })
  }
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }
}
